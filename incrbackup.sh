#!/usr/bin/env bash

cd ${backupdir} 
latestbkp=`find . -name '*.tar' -o -name '*.tar.gz' | sed 's/\([0-9]\{4\}\)/;\1/' | sort -t '\;' -r -k 2,2 | tr -d ';' | head -1`
>/dev/null cd -

if [[ $latestbkp ]]; then
  IFS='_' read -ra DATEBKP <<< "$latestbkp"
  afterthan="${DATEBKP[2]}-${DATEBKP[3]}-${DATEBKP[4]} ${DATEBKP[5]}:${DATEBKP[6]}:${DATEBKP[7]:0:2}"
else
  success 'There are no full backups made to make a incr backup...'
fi

date=`date '+%Y_%m_%d_%H_%M_%S'`
filename=${name}_incr_${date}

tar -cf ${backupdir}/${filename}.tar -T /dev/null

if [[ $ext ]]; then
  IFS=',' read -ra EXT <<< "$ext"
  for i in "${EXT[@]}"; do
    find ${path} -name "*.${i}" -newermt "${afterthan}" | tar -rf ${backupdir}/${filename}.tar --files-from -
  done
else
  find ${path} -newermt "${afterthan}" | tar -rf ${backupdir}/${filename}.tar --files-from -
fi

if [[ ! $(tar -tf ${backupdir}/${filename}.tar) ]]; then
  rm ${backupdir}/${filename}.tar
  die 'ERROR: No files were modified after the last backup'
fi

if [[ $gzip ]]; then
  gzip ${backupdir}/${filename}.tar 
fi

success "Backup ${filename} completed"