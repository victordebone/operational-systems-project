#!/usr/bin/env bash

date=`date '+%Y_%m_%d_%H_%M_%S'`
filename=${name}_${type}_${date}

tar -cf ${backupdir}/${filename}.tar -T /dev/null

if [[ $ext ]]; then
  IFS=',' read -ra EXT <<< "$ext"
  for i in "${EXT[@]}"; do
    find ${path} -name "*.${i}" | tar -rf ${backupdir}/${filename}.tar --files-from -
  done
else
  tar -rf ${backupdir}/${filename}.tar ${path}
fi 


if [[ ! $(tar -tf ${backupdir}/${filename}.tar) ]]; then
  rm ${backupdir}/${filename}.tar
  die 'ERROR: No files found to backup'
fi

if [[ $gzip ]]; then
  gzip ${backupdir}/${filename}.tar 
fi

success "Backup ${filename} completed"