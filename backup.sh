#!/usr/bin/env bash

dir=`dirname $0`

die() {
    printf '%s\n' "$1" >&2
    exit -1
}

success() {
    printf '%s\n' "$1" >&2
    exit 0
}

gdc() {
  a=$1
  b=$2
  r=0
  while :; do
    r=$((a%b))
    a=$((b))
    b=$((r))
    [ "$b" -ne 0 ] || break
  done
  echo $a
}

export -f success die


# backup
# --name='the beginning of the backup file name'
# --full-interval='the time interval between full backups'
# --inc-interval='the time interval between full backup and incremental or incremental backups'
# --path='the directory to archive'
# --gzip='the result will be a file compressed with the program gzip, if this option is used'
# --ext='file extension list; files with specified extensions will be backed up'
# --backup-dir='directory where the backup files will be stored'
#
# Generates backup files:
# [name]_[full|incr]_year_month_day_hour_minute_second.[tar|tar.gz]

# initialization of variables
export path=
export backupdir=.
export name=bkp
export type=full
export ext=
export gzip= 

fullinterval=
incrinterval=

# arguments parsing
while :; do
    case $1 in
        -h|-\?|--help)
            show_help    # Display a usage synopsis.
            exit
            ;;
        --path=?*)
            path=${1#*=} # Delete everything up to "=" and assign the remainder.
            ;;
        --path=)         # Handle the case of an empty --file=
            die 'ERROR: "--path" requires a non-empty option argument.'
            ;;
        --backup-dir=?*)
            backupdir=${1#*=} # Delete everything up to "=" and assign the remainder.
            ;;
        --backup-dir=)         # Handle the case of an empty --file=
            die 'ERROR: "--backup-dir" requires a non-empty option argument.'
            ;;
        --name=?*)
            name=${1#*=} # Delete everything up to "=" and assign the remainder.
            ;;
        --name=)         # Handle the case of an empty --file=
            die 'ERROR: "--name" requires a non-empty option argument.'
            ;;
        --type=?*)
            type=${1#*=} # Delete everything up to "=" and assign the remainder.
            ;;
        --type=)         # Handle the case of an empty --file=
            die 'ERROR: "--type" requires a non-empty option argument.'
            ;;
        --ext=?*)
            ext=${1#*=} # Delete everything up to "=" and assign the remainder.
            ;;
        --ext=)         # Handle the case of an empty --file=
            die 'ERROR: "--ext" requires a non-empty option argument.'
            ;;
        --full-interval=?*)
            fullinterval=${1#*=} # Delete everything up to "=" and assign the remainder.
            ;;
        --full-interval=)         # Handle the case of an empty --file=
            die 'ERROR: "--full-interval" requires a non-empty option argument.'
            ;;
        --incr-interval=?*)
            incrinterval=${1#*=} # Delete everything up to "=" and assign the remainder.
            ;;
        --incr-interval=)         # Handle the case of an empty --file=
            die 'ERROR: "--incr-interval" requires a non-empty option argument.'
            ;;
        --gzip*)
            gzip=true
            ;;
        -?*)
            printf 'WARN: Unknown option (ignored): %s\n' "$1" >&2
            ;;
        *)               # Default case: No more options, so break out of the loop.
            break
    esac

    shift
done

# arguments validation
if [[ ! $path ]]; then
  die 'ERROR: --path argument is obligatory and it must be a valid directory'
fi

if [[ ! -d $path ]]; then
  die 'ERROR: --path must be a valid directory'
fi

if [[ ! $backupdir ]]; then
  die 'ERROR: --backup-dir argument is obligatory and it must be a valid directory'
fi

if [[ ! -d $backupdir ]]; then
  die 'ERROR: --backup-dir must be a valid directory'
fi

if [[ ! ("incr" = $type || "full" = $type) ]]; then
  die 'ERROR: --type must be "full" or "incr"'
fi

case $name in
  *[0-9]*) 
    die 'ERROR: The --name parameter should not contain numbers'
  ;;
  *_*)
    die 'ERROR: The --name parameter should not contain _'
  ;;
esac

case $fullinterval in
  *[!0-9]*) 
    die 'ERROR: The --full-interval parameter should be only seconds'
  ;;
esac

case $incrinterval in
  *[!0-9]*) 
    die 'ERROR: The --incr-interval parameter should be only seconds'
  ;;
esac

if [ $fullinterval ] && [ $fullinterval -lt 0 ]; then
  die 'ERROR: The --full-interval parameter should larger than 0'
fi

if [ $incrinterval ] && [ $incrinterval -lt 0 ]; then
  die 'ERROR: The --incr-interval parameter should larger than 0'
fi

if [ $fullinterval ]  && [ $incrinterval ]; then
  # Zero to run a full first time
  tempfull=0
  tempincr=$incrinterval
  intervalgdc=$(gdc $fullinterval $incrinterval)
fi

while :; do
  if [ $fullinterval ]  && [ $incrinterval ]; then  
    if [ $tempfull -eq 0 ]; then
      $dir/fullbackup.sh
      tempfull=$fullinterval
    fi
    
    if [ $tempfull -ne $fullinterval ] && [ $tempincr -lt 1 ]; then
      $dir/incrbackup.sh
      tempincr=$incrinterval
    fi

    tempfull=$((tempfull-intervalgdc))
    tempincr=$((tempincr-intervalgdc))

    sleep $intervalgdc
  elif [ $fullinterval ]; then
    $dir/fullbackup.sh
    sleep $fullinterval
  elif [ $incrinterval ]; then
    $dir/incrbackup.sh
    sleep $incrinterval
  else 
    $dir/${type}backup.sh
    break
  fi
done

success "Backup script finished"