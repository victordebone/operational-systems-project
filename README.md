# Bash scripts for incremental and differential file backups in Linux

Backup and restore using incremental and full files backups to a user specified time.

## Options

```shell
--name='the beginning of the backup file name'
--full-interval='the time interval between full backups'
--inc-interval='the time interval between full backup and incremental or incremental backups'
--path='the directory to archive'
--gzip='the result will be a file compressed with the program gzip, if this option is used'
--ext='file extension list; files with specified extensions will be backed up'
--backup-dir='directory where the backup files will be stored'
```

As a result in the directory 'backup-dir' full and incremental copies of files from the specified directory will be created.

Copies will be files with the extension 'tar' or 'tgz', if --gzip the option is used.

The backup file name will be as follows:
			 
- full backup: 

```
name_full_year_month_day_hour_minute_second.[tar|tgz]
```

- incremental backup: 

```
name_incr_year_month_day_hour_minute_second.[tar|tgz]
```
           
## Restore backup options

```bash
--name='the beginning of the backup file name'
--date='time to restore the backup (or the closest backup in the past)'
--backup-dir='location of the backup file'
--out-dir='directory to which to extract the backup'
```           

## Tools

- bash and basic shell commands eg. cp, cd, mv, ...
- standard programs like tar, find, grep, awk, ...
- piped file processing