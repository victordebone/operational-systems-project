#!/usr/bin/env bash

die() {
    printf '%s\n' "$1" >&2
    exit -1
}

success() {
    printf '%s\n' "$1" >&2
    exit 0
}

# restore
# --name='the beginning of the backup file name'
# --date='time to restore the backup (or the closest backup in the past)'
# --backup-dir='location of the backup file'
# --out-dir='directory to which to extract the backup'

# initialization of variables
backupdir=
name=bkp
outdir=
date=`date '+%Y-%m-%d %H:%M:%S'`

# arguments parsing
while :; do
    case $1 in
        -h|-\?|--help)
            show_help
            exit
            ;;
        --backup-dir=?*)
            backupdir=${1#*=}
            ;;
        --backup-dir=)
            die 'ERROR: "--backup-dir" requires a non-empty option argument.'
            ;;
        --name=?*)
            name=${1#*=}
            ;;
        --name=)
            die 'ERROR: "--name" requires a non-empty option argument.'
            ;;
        --out-dir=?*)
            outdir=${1#*=}
            ;;
        --out-dir=)
            die 'ERROR: "--out-dir" requires a non-empty option argument.'
            ;;
        --date=?*)
            date=${1#*=}
            if [[ ! $date =~ ^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])\ [0-9][0-9]\:[0-9][0-9]\:[0-9][0-9]$ ]]; then
              die "ERROR: --date ${date} must be a formatted as year-month-day hour:minute:second"
            fi
            ;;
        --date=)
            die 'ERROR: "--date" requires a non-empty option argument.'
            ;;
        -?*)
            printf 'WARN: Unknown option (ignored): %s\n' "$1" >&2
            ;;
        *)
            break
    esac

    shift
done

# arguments validation
if [[ ! $outdir ]]; then
  die 'ERROR: --out-dir argument is obligatory and it must be a valid directory'
fi

if [[ ! -d $outdir ]]; then
  die 'ERROR: --out-dir must be a valid directory'
fi

if [[ ! $backupdir ]]; then
  die 'ERROR: --backup-dir argument is obligatory and it must be a valid directory'
fi

if [[ ! -d $backupdir ]]; then
  die 'ERROR: --backup-dir must be a valid directory'
fi

if [[ $date ]]; then
  latestfullbkp=`find ${backupdir} -name '*.tar' ! -newermt "${date}" -o -name '*.tar.gz' ! -newermt "${date}" | grep -e "${name}_full_" | sed 's/\([0-9]\{4\}\)/;\1/' | sort -t '\;' -r -k 2,2 | tr -d ';' | head -1`
else
  latestfullbkp=`find ${backupdir} -name '*.tar' -o -name '*.tar.gz' | grep -e "${name}_full_" | sed 's/\([0-9]\{4\}\)/;\1/' | sort -t '\;' -r -k 2,2 | tr -d ';' | head -1`
fi

if [[ ! $latestfullbkp ]] && [[ $date ]]; then
  die "There are no full backups made for the name ${name} before ${date}..."
elif [[ ! $latestfullbkp ]]; then
  die "There are no full backups made for the name ${name}..."
fi

tar -xf $latestfullbkp -C ${outdir}
echo "Restored ${latestfullbkp}..."

IFS='/' read -ra PREDATEBKP <<< "$latestfullbkp"
IFS='_' read -ra DATEBKP <<< "${PREDATEBKP[1]}"
afterthan="${DATEBKP[2]}-${DATEBKP[3]}-${DATEBKP[4]} ${DATEBKP[5]}:${DATEBKP[6]}:${DATEBKP[7]:0:2}"

bkplist=`find ${backupdir} -name '*.tar' ! -newermt "${date}" -newermt "${afterthan}" -o -name '*.tar.gz' ! -newermt "${date}" -newermt "${afterthan}" | sed 's/\([0-9]\{4\}\)/;\1/' | sort -t '\;' -k 2,2 | tr -d ';'`

for i in $bkplist; do
  tar -xf $i -C ${outdir}
  echo "Restored ${i}..."
done

success "Restore completed"