#!/usr/bin/env bash

# backup
# --name='the beginning of the backup file name'
# --full-interval='the time interval between full backups'
# --inc-interval='the time interval between full backup and incremental or incremental backups'
# --path='the directory to archive'
# --gzip='the result will be a file compressed with the program gzip, if this option is used'
# --ext='file extension list; files with specified extensions will be backed up'
# --backup-dir='directory where the backup files will be stored'

# restore
# --name='the beginning of the backup file name'
# --date='time to restore the backup (or the closest backup in the past)'
# --backup-dir='location of the backup file'
# --out-dir='directory to which to extract the backup'

./start-tests.sh "Full backup, then incr every 2 seconds, recovering from all after 10 seconds" 

# Initializing first backup
echo "one" > testing-data/one.txt
../backup.sh --path=testing-data --backup-dir=testing-backups

../backup.sh --path=testing-data --backup-dir=testing-backups --incr-interval=2 &
pid=$!
trap 'kill $pid & exit 0' SIGINT SIGTERM EXIT

sleep 2
echo "Modifying data..."
echo "two" >> testing-data/one.txt

sleep 2
echo "Modifying data..."
echo "3" >> testing-data/one.txt

sleep 2
echo "Modifying data..."
echo "4" >> testing-data/one.txt

sleep 2
echo "Modifying data..."
echo "5" >> testing-data/one.txt

sleep 2
echo "Restoring last"
../restore.sh --backup-dir=testing-backups --out-dir=testing-restores