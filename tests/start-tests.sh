#!/usr/bin/env bash

if [[ -d testing-backups ]]; then rm -r testing-backups; fi
if [[ -d testing-restores ]]; then rm -r testing-restores; fi
if [[ -d testing-data ]]; then rm -r testing-data; fi

echo "Starting test $1..."
mkdir testing-backups
mkdir testing-restores
mkdir testing-data